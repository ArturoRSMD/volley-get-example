package com.example.jsonplaceholdervolleyexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        getPost();
    }

    private void getPost(){
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, getString(R.string.base_url), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    //Handle json object
                    JSONObject jsonObject = new JSONObject(response);

                    Log.i("JsonObject ==", response);


                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.i("JsonObject =/=", response);


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        queue.add(stringRequest);
    }

}



